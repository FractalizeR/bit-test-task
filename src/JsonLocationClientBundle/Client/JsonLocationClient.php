<?php

namespace Fractalizer\JsonLocationClientBundle\Client;

use Fractalizer\JsonLocationClientBundle\Data\DTO\LocationCollection;
use Fractalizer\JsonLocationClientBundle\Data\Transformer\V1\LocationCollectionTransformer;
use Fractalizer\JsonLocationClientBundle\Exception\MalformedServerAnswerException;
use Fractalizer\JsonLocationClientBundle\Exception\ProtocolErrorException;
use Fractalizer\JsonLocationClientBundle\Transport\TransportInterface;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;

/**
 * Класс точки входа в клиент. С помощью траспорта соединяется с сервером, получает ответ, осуществляет первичный его
 * разбор и передачу объектам переноса данных для парсинга
 */
class JsonLocationClient implements JsonLocationClientInterface
{
    /**
     * @var TransportInterface
     */
    private $transport;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var bool
     */
    private $trackPerformance = false;

    /**
     * @var array
     */
    private $performanceData = [];

    /**
     * @param TransportInterface $transport
     * @param LoggerInterface $logger
     */
    public function __construct(TransportInterface $transport, LoggerInterface $logger)
    {
        $this->transport = $transport;
        $this->logger = $logger;

        $this->logger->debug(__CLASS__.': Using transport '.get_class($this->transport));
    }

    /**
     * @return LocationCollection
     * @throws MalformedServerAnswerException
     * @throws ProtocolErrorException
     */
    public function getLocationsData(): LocationCollection
    {
        $started = microtime(true);
        $serverResponse = $this->transport->getLocationsResponse();
        $finished = microtime(true);

        if ($this->trackPerformance) {
            $this->performanceData[] = [__METHOD__ => ['started' => $started, 'finished' => $finished]];
        }


        $data = $this->decodeServerResponse($serverResponse);
        $this->logger->debug(__CLASS__.": Got server response: $serverResponse");

        if (!isset($data['success'])) {
            throw new MalformedServerAnswerException(
                "Server reply should contain 'success' field. Current reply is '$serverResponse'"
            );
        }

        if (!$data['success']) {
            throw new ProtocolErrorException(
                $data['data']['message'] ?? '[Empty server error message]',
                $data['data']['code'] ?? '[Empty server error code]'
            );
        }

        $locationsCollectionData = new LocationCollectionTransformer($data['data']['locations']);

        try {
            return $locationsCollectionData->transform();
        } catch (InvalidArgumentException $e) {
            throw new MalformedServerAnswerException(
                "Server data is malformed. Details: {$e->getMessage()}",
                $e->getCode(),
                $e
            );
        }
    }

    /**
     * @param string $serverResponse
     *
     * @return array
     * @throws MalformedServerAnswerException
     */
    private function decodeServerResponse(string $serverResponse): array
    {
        try {
            $responseBody = \GuzzleHttp\json_decode($serverResponse, true);
        } catch (InvalidArgumentException $e) {
            throw new MalformedServerAnswerException(
                "Server was expected to return json data, but returned '{$serverResponse}"
            );
        }

        return $responseBody;
    }

    /**
     * @param bool $value
     */
    public function setTrackPerformance(bool $value = true): void
    {
        $this->trackPerformance = $value;
    }

    /**
     * @return array
     */
    public function getPerformanceData(): array
    {
        return $this->performanceData;
    }
}
