Неймспейс, содержащий точку входа в клиент. Содержит публично-доступный API для получения списка местоположений.

Все обращения к клиенту из кода основного приложения лучше вести через интерфейс для облегчения подмены реализации при необходимости (например в тестах)

Транспорт инжектируется в клиент через IoC контейнер.

Реализовано базовое логирование через Monolog и базовый трекинг производительности работы для профайлера Symfony (возможно, потом потребуется реализация дата-коллектора для панели). Для включения трекинга производительности можно вызвать метод setTrackPerformance напрямую или через контейнер.