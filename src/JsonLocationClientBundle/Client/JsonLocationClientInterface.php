<?php

namespace Fractalizer\JsonLocationClientBundle\Client;

use Fractalizer\JsonLocationClientBundle\Data\DTO\LocationCollection;
use Fractalizer\JsonLocationClientBundle\Exception\MalformedServerAnswerException;

/**
 * Интерфейс клиента. Всю коммуникацию с клиентом лучше вести через интерфейс, а не через класс, для того, чтобы
 * впоследствии при тестах клиент можно было легко подменить другим классом
 */
interface JsonLocationClientInterface
{
    /**
     * @return LocationCollection
     * @throws MalformedServerAnswerException
     */
    public function getLocationsData(): LocationCollection;

    /**
     * @param bool $value
     */
    public function setTrackPerformance(bool $value = true);

    /**
     * @return array
     */
    public function getPerformanceData(): array;
}
