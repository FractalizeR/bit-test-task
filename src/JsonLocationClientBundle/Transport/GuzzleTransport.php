<?php

namespace Fractalizer\JsonLocationClientBundle\Transport;

use Fractalizer\JsonLocationClientBundle\Exception\ServerConnectionException;
use GuzzleHttp\Client as GuzzleHttpClient;
use GuzzleHttp\Exception\TransferException;
use Psr\Log\LoggerInterface;

/**
 * Класс для обращения к серверу через библиотеку Guzzle
 */
class GuzzleTransport implements TransportInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var GuzzleHttpClient
     */
    private $client;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string $host
     * @param LoggerInterface $logger
     */
    public function __construct(string $host, LoggerInterface $logger)
    {
        $this->host = $host;
        $this->logger = $logger;

        $this->client = new GuzzleHttpClient(
            [
                'base_uri'    => $this->host,
                'http_errors' => true,
            ]
        );
    }

    /**
     * @return string
     * @throws ServerConnectionException
     */
    public function getLocationsResponse(): string
    {
        $this->logger->debug(__CLASS__ . ": Requesting url: {$this->host}/locations");

        try {
            $response = $this->client->request('GET', 'locations');
        } catch (TransferException $e) {
            throw new ServerConnectionException(
                "There was a problem connecting to '{$this->host}': {$e->getMessage()}",
                $e->getCode(), $e
            );
        }

        return $response->getBody()->getContents();
    }
}
