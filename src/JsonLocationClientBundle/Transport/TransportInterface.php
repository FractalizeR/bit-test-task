<?php

namespace Fractalizer\JsonLocationClientBundle\Transport;

use Fractalizer\JsonLocationClientBundle\Exception\ServerConnectionException;

/**
 * Интерфейс, который должны реализовывать все классы траспорта
 */
interface TransportInterface
{
    /**
     * @return string
     * @throws ServerConnectionException
     */
    public function getLocationsResponse(): string;
}
