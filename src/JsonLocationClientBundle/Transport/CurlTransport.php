<?php

namespace Fractalizer\JsonLocationClientBundle\Transport;

use Exception;
use Fractalizer\JsonLocationClientBundle\Exception\ServerConnectionException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Класс для обращения к серверу через curl
 */
class CurlTransport implements TransportInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param string $host
     * @param LoggerInterface $logger
     */
    public function __construct(string $host, LoggerInterface $logger)
    {
        $this->host = $host;
        $this->logger = $logger;
    }

    /**
     * @return string
     */
    public function getLocationsResponse(): string
    {
        $ch = $this->getCurlHandle();

        $this->setCurlOption($ch, CURLOPT_RETURNTRANSFER, true);
        $this->setCurlOption($ch, CURLOPT_URL, $this->host.'/locations');
        $this->logger->debug(__CLASS__.": Requesting url: {$this->host}/locations");

        $result = $this->fetchResponseFromServer($ch);
        curl_close($ch);

        return $result;
    }

    /**
     * @return resource
     * @throws Exception
     */
    private function getCurlHandle(): resource
    {
        $ch = curl_init();

        if (!is_resource($ch)) {
            throw new Exception("System error. Cannot initialize Curl subsystem");
        }

        return $ch;
    }

    /**
     * @param     $handle
     * @param int $option
     * @param     $value
     *
     * @throws Exception
     */
    private function setCurlOption($handle, int $option, $value): void
    {
        $result = curl_setopt($handle, $option, $value);
        if ($result !== true) {
            throw new Exception("System error. Cannot set curl option {$option} to value {$value}");
        }
    }

    /**
     * @param resource $ch
     * @return string
     * @throws ServerConnectionException
     */
    private function fetchResponseFromServer($ch): string
    {
        $result = curl_exec($ch);

        if (false === $result || (Response::HTTP_OK !== curl_getinfo($ch, CURLINFO_HTTP_CODE))) {
            $info = curl_getinfo($ch);
            throw new ServerConnectionException(
                "Error while connecting to server: ".curl_error($ch)."\nAdditional info: ".print_r($info, true)
            );
        }

        return $result;
    }
}
