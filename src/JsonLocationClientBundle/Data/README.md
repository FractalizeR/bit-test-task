Неймспейс содержит классы для работы с данными клиента.

 - Client - клиентское представление данных. Эти объекты будут отданы коду основного приложения при вызове клиента. В них же выполняется валидация данных
 - Transformer - парсинг серверного представление данных. Эти классы умеют разбирать данные, поступившие от сервера в виде простых массивов и формировать из них клиентское представление