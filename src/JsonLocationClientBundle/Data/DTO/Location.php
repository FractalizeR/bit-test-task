<?php

namespace Fractalizer\JsonLocationClientBundle\Data\DTO;

/**
 * Класс для представления местоположения с именем и координатами
 */
class Location
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Coordinates
     */
    private $coordinates;

    /**
     * @param string $name
     * @param Coordinates $coordinates
     */
    public function __construct(string $name, Coordinates $coordinates)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException("Name can't be empty. '$name' given");
        }

        $this->name = $name;
        $this->coordinates = $coordinates;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Coordinates
     */
    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }
}
