<?php

namespace Fractalizer\JsonLocationClientBundle\Data\DTO;

/**
 * Класс для представления географических координат
 */
class Coordinates
{
    /**
     * @var string
     */
    private $lat;

    /**
     * @var string
     */
    private $long;

    /**
     * Coordinate constructor.
     *
     * @param string $lat  Latitude
     * @param string $long Longitude
     */
    public function __construct(string $lat, string $long)
    {
        $this->assertParamsCorrect($lat, $long);

        $this->lat = $lat;
        $this->long = $long;
    }

    /**
     * @param string $lat
     * @param string $long
     */
    private function assertParamsCorrect(string $lat, string $long): void
    {
        if (!is_numeric($lat) || $lat < -90 || $lat > 90) {
            throw new \InvalidArgumentException("Latitude should be a number from -90 to +90. '$lat' Given");
        }

        if (!is_numeric($long) || $long < -180 || $long > 180) {
            throw new \InvalidArgumentException("Longitude should be a number from -180 to +180. '$long' Given");
        }
    }

    /**
     * @return string
     */
    public function getLat(): string
    {
        return $this->lat;
    }

    /**
     * @return string
     */
    public function getLong(): string
    {
        return $this->long;
    }
}
