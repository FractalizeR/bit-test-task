<?php

namespace Fractalizer\JsonLocationClientBundle\Data\DTO;

/**
 * Класс для представления коллекции местоположений
 */
class LocationCollection implements \Countable, \IteratorAggregate, \ArrayAccess
{
    /**
     * @var array
     */
    private $locationCollection = [];

    /**
     * @param Location $location
     */
    public function add(Location $location): void
    {
        $this->locationCollection[] = $location;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->locationCollection);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->locationCollection);
    }

    /**
     * @param int|string $offset
     * @param Location   $value
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->locationCollection[] = $value;
        } else {
            $this->locationCollection[$offset] = $value;
        }
    }

    /**
     * @param int|string $offset
     *
     * @return bool
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * @param int|string $offset
     */
    public function offsetUnset($offset)
    {
        unset($this->locationCollection[$offset]);
    }

    /**
     * @param int|string $offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->locationCollection[$offset] : null;
    }
}
