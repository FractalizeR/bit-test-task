<?php

namespace Fractalizer\JsonLocationClientBundle\Data\Transformer\V1;

use Fractalizer\JsonLocationClientBundle\Data\DTO\Coordinates;

/**
 * Класс для парсинга координат
 */
class CoordinatesTransformer
{
    /**
     * @var array
     */
    private $coordinatesData;

    public function __construct(array $coordinatesData)
    {
        $this->coordinatesData = $coordinatesData;
    }

    public function transform(): Coordinates
    {
        if (!isset($this->coordinatesData['lat']) || !isset($this->coordinatesData['long'])) {
            throw new \InvalidArgumentException(
                "Latitude or longitude data not found. Given: " . print_r($this->coordinatesData, true)
            );
        }

        return new Coordinates($this->coordinatesData['lat'], $this->coordinatesData['long']);
    }
}
