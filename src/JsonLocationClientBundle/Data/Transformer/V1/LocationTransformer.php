<?php

namespace Fractalizer\JsonLocationClientBundle\Data\Transformer\V1;

use Fractalizer\JsonLocationClientBundle\Data\DTO\Location;

/**
 * Класс для парсинга местоположений
 */
class LocationTransformer
{
    /**
     * @var array
     */
    private $locationData;

    public function __construct(array $locationData)
    {
        $this->locationData = $locationData;
    }

    public function transform(): Location
    {
        if (!isset($this->locationData['name'])) {
            throw new \InvalidArgumentException(
                "Location name not found. Given: ".print_r($this->locationData, true)
            );
        }
        $name = $this->locationData['name'];
        $coordinates = new CoordinatesTransformer($this->locationData['coordinates']);

        return new Location($name, $coordinates->transform());
    }
}
