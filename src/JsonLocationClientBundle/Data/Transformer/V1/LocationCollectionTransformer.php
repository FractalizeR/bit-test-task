<?php

namespace Fractalizer\JsonLocationClientBundle\Data\Transformer\V1;

use Fractalizer\JsonLocationClientBundle\Data\DTO\LocationCollection;

/**
 * Класс для парсинга коллекции местоположений
 */
class LocationCollectionTransformer
{
    /**
     * @var array
     */
    private $locationCollectionData;

    /**
     * @param array $locationCollectionData
     */
    public function __construct(array $locationCollectionData)
    {
        $this->locationCollectionData = $locationCollectionData;
    }

    public function transform(): LocationCollection
    {
        $result = new LocationCollection();

        if (!is_array($this->locationCollectionData)) {
            throw new \InvalidArgumentException(
                "Locations data is not an array. Given: ".print_r($this->locationCollectionData, true)
            );
        }

        $locationIndex = 0;
        foreach ($this->locationCollectionData as $locationCollectionDatum) {
            $location = new LocationTransformer($locationCollectionDatum);
            try {
                $result->add($location->transform());
            } catch (\InvalidArgumentException $e) {
                throw new \InvalidArgumentException(
                    "Error parsing location with index $locationIndex: {$e->getMessage()}", $e->getCode(), $e
                );
            }
            $locationIndex++;
        }

        return $result;
    }
}
