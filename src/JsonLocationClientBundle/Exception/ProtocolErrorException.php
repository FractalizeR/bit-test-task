<?php

namespace Fractalizer\JsonLocationClientBundle\Exception;

class ProtocolErrorException extends \Exception
{
    /**
     * @var string
     */
    private $serverErrorCode;

    /**
     * ProtocolErrorException constructor.
     *
     * @param string $message
     * @param string $serverErrorCode
     */
    public function __construct(string $message, string $serverErrorCode)
    {
        parent::__construct($message);
        $this->serverErrorCode = $serverErrorCode;
    }

    /**
     * @return string
     */
    public function getServerErrorCode(): string
    {
        return $this->serverErrorCode;
    }
}