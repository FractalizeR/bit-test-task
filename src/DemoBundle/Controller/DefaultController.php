<?php

namespace Fractalizer\DemoBundle\Controller;

use Fractalizer\JsonLocationClientBundle\Client\JsonLocationClientInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * Роут, для отображения коллекции, полученной от locationsAction
     *
     * @Route("/")
     * @param JsonLocationClientInterface $client
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(JsonLocationClientInterface $client)
    {
        $locationsData = $client->getLocationsData();

        return $this->render(
            'FractalizerDemoBundle:Default:index.html.twig',
            ['locationsData' => $locationsData]
        );
    }

    /**
     * Тестовый метод, эмулирующий работу удаленного API
     *
     * @Route("/locations")
     */
    public function locationsAction()
    {
        return $this->json(
            [
                "data"    => [
                    "locations" => [
                        [
                            "name"        => "Eiffel Tower",
                            "coordinates" => [
                                "lat"  => 21.12,
                                "long" => 19.56,
                            ],
                        ],
                    ],
                ],
                "success" => true,
            ]
        );
    }
}
