<?php

namespace Fractalizer\Tests\JsonLocationClientBundle\Functional;

use Fractalizer\JsonLocationClientBundle\Client\JsonLocationClient;
use Fractalizer\JsonLocationClientBundle\Transport\TransportInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\VarDumper\Test\VarDumperTestTrait;

/**
 * Простой тест работы клиента с корректными данными
 */
class ClientPositiveTest extends WebTestCase
{
    use VarDumperTestTrait;

    public function testClientSuccess()
    {
        $transport = $this->getGoodTransport();

        /** @var \Psr\Log\LoggerInterface $logger */
        $logger = static::createClient()->getContainer()->get('logger');

        $client = new JsonLocationClient($transport, $logger);

        $locations = $client->getLocationsData();

        $this->assertDumpEquals(str_replace("\r\n", "\n", $this->getGoodDump()), $locations);
    }

    /**
     * Мокаем транспорт, чтобы избежать обращения к серверу
     *
     * @return TransportInterface
     */
    private function getGoodTransport(): TransportInterface
    {
        return new class implements TransportInterface
        {
            public function getLocationsResponse(): string
            {
                return json_encode(
                    [
                        "data" => [
                            "locations" => [
                                [
                                    "name" => "Eiffel Tower",
                                    "coordinates" => [
                                        "lat" => 21.12,
                                        "long" => 19.56,
                                    ],
                                ],
                            ],
                        ],
                        "success" => true,
                    ]
                );
            }
        };
    }

    /**
     * Дамп для сравнения
     * @return string
     */
    private function getGoodDump()
    {
        return <<<EOTXT
Fractalizer\JsonLocationClientBundle\Data\DTO\LocationCollection {
  -locationCollection: array:1 [
    0 => Fractalizer\JsonLocationClientBundle\Data\DTO\Location {
      -name: "Eiffel Tower"
      -coordinates: Fractalizer\JsonLocationClientBundle\Data\DTO\Coordinates {
        -lat: "21.12"
        -long: "19.56"
      }
    }
  ]
}
EOTXT;
    }
}