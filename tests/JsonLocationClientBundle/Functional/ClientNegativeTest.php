<?php

namespace Fractalizer\Tests\JsonLocationClientBundle\Functional;

use Fractalizer\JsonLocationClientBundle\Client\JsonLocationClient;
use Fractalizer\JsonLocationClientBundle\Exception\MalformedServerAnswerException;
use Fractalizer\JsonLocationClientBundle\Exception\ProtocolErrorException;
use Fractalizer\JsonLocationClientBundle\Transport\TransportInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\VarDumper\Test\VarDumperTestTrait;

/**
 * Негативный тест клиента для проверки реакции на ошибки
 */
class ClientNegativeTest extends WebTestCase
{
    use VarDumperTestTrait;

    /**
     * Проверка ошибки протокола на сервере
     */
    public function testClientServerError()
    {
        $transport = $this->getServerErrorTransport();

        /** @var \Psr\Log\LoggerInterface $logger */
        $logger = static::createClient()->getContainer()->get('logger');

        $client = new JsonLocationClient($transport, $logger);

        $this->expectException(ProtocolErrorException::class);
        $this->expectExceptionMessage("Error on server");
        $client->getLocationsData();
    }

    /**
     * Проверка некорректных данных при парсинге
     */
    public function testClientMalformedDataError()
    {
        $transport = $this->getServerMalformedMessageTransport();

        /** @var \Psr\Log\LoggerInterface $logger */
        $logger = static::createClient()->getContainer()->get('logger');

        $client = new JsonLocationClient($transport, $logger);

        $this->expectException(MalformedServerAnswerException::class);
        $this->expectExceptionMessageRegExp(
            '/Error parsing location with index 0: Latitude should be a number from -90 to \+90/i'
        );
        $client->getLocationsData();
    }

    /**
     * Мокаем транспорт, чтобы избежать обращения к серверу
     *
     * @return TransportInterface
     */
    private function getServerErrorTransport(): TransportInterface
    {
        return new class implements TransportInterface
        {
            public function getLocationsResponse(): string
            {
                return json_encode(
                    [
                        "data" => [
                            "message" => "Error on server",
                            "code" => "100",
                        ],
                        "success" => false,
                    ]
                );
            }
        };
    }

    /**
     * Мокаем транспорт, чтобы избежать обращения к серверу
     *
     * @return TransportInterface
     */
    private function getServerMalformedMessageTransport(): TransportInterface
    {
        return new class implements TransportInterface
        {
            public function getLocationsResponse(): string
            {
                return json_encode(
                    [
                        "data" => [
                            "locations" => [
                                [
                                    "name" => "Eiffel Tower",
                                    "coordinates" => [
                                        "lat" => -10000, // Некорректная величина
                                        "long" => 19.56,
                                    ],
                                ],
                            ],
                        ],
                        "success" => true,
                    ]
                );
            }
        };
    }
}