<?php

namespace Fractalizer\Tests\JsonLocationClientBundle\Unit\Data\Transformer;

use Fractalizer\JsonLocationClientBundle\Data\Transformer\V1\CoordinatesTransformer;
use PHPUnit\Framework\TestCase;

/**
 * Тест конвертации координат
 */
class CoordinatesTransformerTest extends TestCase
{
    public function testPositive()
    {
        $data = ['lat' => 10, 'long' => 11];
        $obj = (new CoordinatesTransformer($data))->transform();
        $this->assertEquals(10, $obj->getLat());
        $this->assertEquals(11, $obj->getLong());
    }

    public function testNegative()
    {
        $this->expectException(\InvalidArgumentException::class);
        $data = ['latIncorrect' => 10, 'longIncorrect' => 11];
        (new CoordinatesTransformer($data))->transform();
    }
}
