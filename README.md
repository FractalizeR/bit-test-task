Решение тестового задания.
====================================

Решение
----------------

Решение реализовано ввиде полной установки Symfony 3. Изменения вносились в параметры, роутинг и сервисы оригинального приложения. Бандл имеет свою конфигурацию служб.

 - Бандл решения находится тут: `src\JsonLocationClientBundle`. В каждой папке есть README.md, описывающий содержимое и принятые архитектурные решения.
 - Тест его работы `src\DemoBundle\Controller\DefaultController.php`
 - Функциональные и юнит тесты для клиента: `tests\JsonLocationClientBundle\Functional`. Тестов должно быть больше, но весь набор я реализовывать не стал, ограничился только базовыми, которые необходимы в любом случае. Тесты запускаются простым вызовом `phpunit` и не требуют наличия интернет-соединения или эмуляции HTTP сервера.

Условие:
---------------------------

Need to implement Symfony3 bundle for getting JSON-encoded locations data stored in predefined format.

Acceptance criteria:
1. Client should be defined as a service class in a bundle config;
1. Client should utilize CURL as a transport layer (can rely upon any third-party bundle however should be implemented as a separate class/package);
1. Properly defined exceptions should be thrown on CURL errors, malformed JSON response and error JSON response;
1. Resulting data should be fetched as an array (or other collection) of properly defined PHP objects.

JSON response format:

```json
{
    “"data"”: {
        “"locations”": [
            {
                “"name”": “"Eiffel Tower”",
                “"coordinates"”: {
                    “"lat”": 21.12,
                    “"long”": 19.56
                }
            },
            ...
        ]
    },
    “"success"”: true
}
```
JSON error response format:

```json
{
    “"data"”: {
        “"message"”: "string error message",
        “"code"”: "string error code"
    },
    “"success"”: false
}
```

